class Invoice():

    @property
    def new_invoice_obj(self):
        """
        USAGE:
        invoice_obj = Invoice().new_invoice_obj
        """
        return {
            "D14": "E14xxx",
            "E15": "E15xxx",
            "E16": "E16xxx",
            "E17": "E17xxx",
            "D15": "D15xxx",
            "D14": "D14xxx",
            "D17": "D17xxx",
            "D16": "D16xxx",
            "C16": "C16xxx",
            "C17": "C17xxx",
            "C14": "C14xxx",
            "C15": "C15xxx",
            "C11": "C11xxx",
            'B11': 'B11xxx',
            "B17": "B17xxx",
            "B16": "B16xxx",
            "B15": "B15xxx",
            "B14": "B14xxx",
            'A11': 'A11xxx',
            "D5": "D5xxx",
            'D4': 'D4xxx',
            'D7': 'D7xxx',
            'D6': 'D6xxx',
            "B7": "B7xxx",
            "B6": "B6xxx",
            "B5": "B5xxx",
            "B4": "B4xxx",
            "B3": "B3xxx",
            "G3": "G3xxx",
            "G4": "G4xxx",
            "G5": "G5xxx",
            "G6": "G6xxx",
            "G7": "G7xxx",
            'C4': 'C4xxx',
            'C5': 'C5xxx',
            'C6': 'C6xxx',
            "C7": "C7xxx",
            'D3': 'D3xxx',
            'C3': 'C3xxx'
        }
