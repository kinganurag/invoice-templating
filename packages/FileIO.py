import shutil


def copyFile(src, dest):
    """
    src = {0: "CreativeAssumptions_SampleInvoice.xlsx", }
    dest = {0: "dump/"}
    copyFile(src[0], dest[0])

    """
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)
