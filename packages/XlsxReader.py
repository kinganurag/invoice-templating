import openpyxl as px


class XlsxReader():

    def _init():
        W = px.load_workbook(
            'CreativeAssumptions_SampleInvoice.xlsx')
        print repr(W), "\n"
        # for _ in W:
        #     print _
        # print _.ReadOnlyWorksheet

        # p = W.get_sheet_by_name()
        sheet_names = W.get_sheet_names()
        p = W.get_sheet_by_name(name=sheet_names[0])

    def reading_background_color(self):
        fields_to_be_replaced = {}

        for row in p.iter_rows():
            # print "\nrow: ", repr(row)
            for cell in row:
                i = cell.fill.start_color.index  # Green Color
                print "cell", cell.coordinate, "i", i, type(i)
                if i == "FFFFFF00":
                    fields_to_be_replaced[cell.coordinate] = cell.value

    def change_background_color(self):
        """
        from openpyxl.styles import Color, Fill
        class Color(HashableObject):
            # "Named colors for use in styles."
            BLACK = 'FF000000'
            WHITE = 'FFFFFFFF'
            RED = 'FFFF0000'
            DARKRED = 'FF800000'
            BLUE = 'FF0000FF'
            DARKBLUE = 'FF000080'
            GREEN = 'FF00FF00'
            DARKGREEN = 'FF008000'
            YELLOW = 'FFFFFF00'
            DARKYELLOW = 'FF808000'
            book = load_workbook('foo.xlsx')

            # define ws here, in this case I pick the first worksheet in the workbook...
            #    NOTE: openpyxl has other ways to select a specific worksheet (i.e. by name
            #    via book.get_sheet_by_name('someWorksheetName'))
            ws = book.worksheets[0]

            ## ws is a openpypxl worksheet object
            _cell = ws.cell('C1')
            _cell.style.fill.fill_type = Fill.FILL_SOLID
            _cell.style.fill.start_color.index = Color.DARKRED

        """
        pass
