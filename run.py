"""
# Doubt:
    Need to preserve the formula

# Good link:
https://automatetheboringstuff.com/chapter12/

# Limitation:
1. Differentiating between merge column (no now)

# Todo:
1. read the names of xlsx file in the folder
"""


import openpyxl as px
from packages.FileIO import copyFile
from model.Mapper import Mapper
from model.Invoice import Invoice
from openpyxl.styles import Font, colors, PatternFill

W = px.load_workbook(
    'xls/CreativeAssumptions_SampleInvoice.xlsx')
print repr(W), "\n"
sheet_names = W.get_sheet_names()
sheet = W.get_sheet_by_name(name=sheet_names[0])
print repr(sheet)

a = []
fields_to_be_replaced = {}

# Reading the marked cells
for row in sheet.iter_rows():
    for cell in row:
        i = cell.fill.start_color.index  # Green Color
        if i == "FFFFFF00":
            fields_to_be_replaced[cell.coordinate] = cell.value

print repr(fields_to_be_replaced)

# Getting the mapping
mapping_dict = Mapper().mapping

# Getting the Invoice object
invoice_obj = Invoice().new_invoice_obj


replace_obj = {}
for key, value in mapping_dict.iteritems():
    replace_obj[key] = invoice_obj[value]

print repr(replace_obj["C16"])

# ------ Getting the template
# src = {0: "CreativeAssumptions_SampleInvoice.xlsx", }
# dest = {0: "modified_invoice/"}
# copyFile(src[0], dest[0])
# -------


# Populatting the fields with informaton from our Invoice data object
ft = Font(color=colors.RED)
pfill = PatternFill(fill_type="solid",
                    start_color='FFFFFFFF',
                    end_color='FF000000')
# ---
for key, values in replace_obj.iteritems():
    sheet[key] = values
    sheet[key].font = ft
    sheet[key].fill = pfill

W.save('final_invoices/final_invoice.xlsx')

